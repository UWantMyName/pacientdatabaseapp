﻿CREATE TABLE [dbo].[Patients] (
    [Id]        INT  IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR(150) NULL,
    [LastName]  VARCHAR(150) NULL,
    [Age]       INT  NULL,
    [Gender]    INT  NULL,
    [Residence] VARCHAR(100) NULL,
    [Simptoms]  VARCHAR(300) NULL,
    [Diagnosis] VARBINARY(200) NULL,
    [DayOR]     INT  NULL,
    [MonthOR]   VARBINARY(20) NULL,
    [YearOR]    INT  NULL,
    [Treatment] VARBINARY(300) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'First Name of Pacient', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'FirstName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Last Name(s) of Patient', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'LastName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Age of Patient', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'Age';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1 for male, 0 for female', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'Gender';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Residence of Patient', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'Residence';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Simptoms the Patient had', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'Simptoms';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Diagnosis of the Patient', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'Diagnosis';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Day of Registration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'DayOR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Month of Registration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'MonthOR';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Year of Registration', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'Patients', @level2type = N'COLUMN', @level2name = N'YearOR';

