﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacientDatabaseApp
{
    public partial class NewPatient : Form
    {
		//string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='D:\STEVEN STUFF\PROGRAMARE\C#\PACIENTDATABASEAPP\PacientDatabaseApp\PacientDatabaseApp\Patients.mdf';Integrated Security=True";
		string ConnectionString;

		public NewPatient()
        {
            InitializeComponent();

			string path = Path.GetDirectoryName(Path.GetDirectoryName(Application.StartupPath));
			ConnectionString = "Data Source=(LocalDB)" + "\\" + "MSSQLLocalDB;AttachDbFilename=" + "\"" + path + "\\Patients.mdf" + "\"" + ";Integrated Security=True";


			LoadForm();
        }

        private bool Bisect(int year)
        {
            if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) return true;
            return false;
        }

        private bool CheckDate(int day, int month, int year)
        {
            if (month == 4 || month == 6 || month == 9 || month == 11)
            {
                if (day > 30) return false;
            }
            if (month == 2)
            {
                if (Bisect(year) && day > 29) return false;
                if (!Bisect(year) && day > 28) return false;
            }
            return true;
        }

        private void OK_Button_Click(object sender, EventArgs e)
        {
            try
            {
                string comm;

                if (CheckDate(int.Parse(Day_ComboBox.Text),MonthValue(Month_ComboBox.Text),int.Parse(Year_ComboBox.Text)))
                {
                    comm = "insert into Patients (FirstName, LastName, Age, Gender, Residence, Simptoms, Diagnosis, DayOR, MonthOR, YearOR, Treatment) VALUES (@FirstName, @LastName, @Age, @Gender, @Residence, @Simptoms, @Diagnosis, @DayOR, @MonthOR, @YearOR, @Treatment)";
                    using (var cn = new SqlConnection(ConnectionString))
                    {
                        cn.Open();
                        using (SqlCommand Command = new SqlCommand(comm, cn))
                        {
                            Command.CommandText = "insert into Patients (FirstName, LastName, Age, Gender, Residence, Simptoms, Diagnosis, DayOR, MonthOR, YearOR, Treatment) VALUES (@FirstName, @LastName, @Age, @Gender, @Residence, @Simptoms, @Diagnosis, @DayOR, @MonthOR, @YearOR, @Treatment)";
                            Command.Parameters.AddWithValue("@FirstName", FirstName_TextBox.Text);
                            Command.Parameters.AddWithValue("@LastName", LastName_TextBox.Text);
                            Command.Parameters.AddWithValue("@Age", int.Parse(Age_TextBox.Text));
                            if (Gender_ComboBox.Text == "Female") Command.Parameters.AddWithValue("@Gender", "0");
                            else Command.Parameters.AddWithValue("@Gender", "1");
                            Command.Parameters.AddWithValue("@Residence", Residence_TextBox.Text);
                            Command.Parameters.AddWithValue("@Simptoms", Simptoms_TextBox.Text);
                            Command.Parameters.AddWithValue("@Diagnosis", Diagnosis_TextBox.Text);
                            Command.Parameters.AddWithValue("@DayOR", int.Parse(Day_ComboBox.Text));
                            Command.Parameters.AddWithValue("@MonthOR", Month_ComboBox.Text);
                            Command.Parameters.AddWithValue("@YearOR", int.Parse(Year_ComboBox.Text));
                            Command.Parameters.AddWithValue("@Treatment", Treatment_TextBox.Text);
                            Command.ExecuteNonQuery();
                        }
                        cn.Close();
                    }

                    MessageBox.Show("Pacient successfully registered!");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("The date of registration is not valid.");
                }
            }
            catch
            {
                MessageBox.Show("Patient could not be registered.");
            }
        }

        private int MonthValue(string Month)
        {
            if (Month == "January") return 1;
            if (Month == "February") return 2;
            if (Month == "March") return 3;
            if (Month == "April") return 4;
            if (Month == "May") return 5;
            if (Month == "June") return 6;
            if (Month == "July") return 7;
            if (Month == "August") return 8;
            if (Month == "September") return 9;
            if (Month == "October") return 10;
            if (Month == "November") return 11;
            return 12;
        }

        private void LoadForm()
        {
            int i;

            for (i = 1; i <= 31; i++)
                Day_ComboBox.Items.Add(i.ToString());

            Month_ComboBox.Items.Add("Ianuarie");
            Month_ComboBox.Items.Add("Februarie");
            Month_ComboBox.Items.Add("Martie");
            Month_ComboBox.Items.Add("Aprilie");
            Month_ComboBox.Items.Add("Mai");
            Month_ComboBox.Items.Add("Iunie");
            Month_ComboBox.Items.Add("Iulie");
            Month_ComboBox.Items.Add("August");
            Month_ComboBox.Items.Add("Septembrie");
            Month_ComboBox.Items.Add("Octombrie");
            Month_ComboBox.Items.Add("Noiembrie");
            Month_ComboBox.Items.Add("Decembrie");

            for (i = 2000; i <= DateTime.Today.Year; i++)
                Year_ComboBox.Items.Add(i.ToString());
        }

        private void CurrentDate_Button_Click(object sender, EventArgs e)
        {
            DateTime Data = DateTime.Today;
            Day_ComboBox.Text = Data.Day.ToString();
            Month_ComboBox.Text = Data.Month.ToString();
            Year_ComboBox.Text = Data.Year.ToString();
        }

        private void NotSpecified_Button_Click(object sender, EventArgs e)
        {
            Treatment_TextBox.Text = "Nu este cazul.";
        }

        private void NotSpecifiedSimptom_Button_Click(object sender, EventArgs e)
        {
            Simptoms_TextBox.Text = "Nu este cazul.";
        }

		private void NewPatient_Load(object sender, EventArgs e)
		{

		}
	}
}
