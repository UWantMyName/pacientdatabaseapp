﻿namespace PacientDatabaseApp
{
    partial class NewPatient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NewPatient));
			this.FirstName_Label = new System.Windows.Forms.Label();
			this.LastName_Label = new System.Windows.Forms.Label();
			this.Age_Label = new System.Windows.Forms.Label();
			this.Diagnosis_Label = new System.Windows.Forms.Label();
			this.FirstName_TextBox = new System.Windows.Forms.TextBox();
			this.LastName_TextBox = new System.Windows.Forms.TextBox();
			this.Age_TextBox = new System.Windows.Forms.TextBox();
			this.OK_Button = new System.Windows.Forms.Button();
			this.Simptoms_Label = new System.Windows.Forms.Label();
			this.Simptoms_TextBox = new System.Windows.Forms.TextBox();
			this.Gender_Label = new System.Windows.Forms.Label();
			this.Gender_ComboBox = new System.Windows.Forms.ComboBox();
			this.Residence_Label = new System.Windows.Forms.Label();
			this.Residence_TextBox = new System.Windows.Forms.TextBox();
			this.DateOfRegistration_Label = new System.Windows.Forms.Label();
			this.CurrentDate_Button = new System.Windows.Forms.Button();
			this.Day_ComboBox = new System.Windows.Forms.ComboBox();
			this.Month_ComboBox = new System.Windows.Forms.ComboBox();
			this.Year_ComboBox = new System.Windows.Forms.ComboBox();
			this.DD_Label = new System.Windows.Forms.Label();
			this.MM_Label = new System.Windows.Forms.Label();
			this.YY_Label = new System.Windows.Forms.Label();
			this.Treatment_Label = new System.Windows.Forms.Label();
			this.Treatment_TextBox = new System.Windows.Forms.TextBox();
			this.NotSpecifiedTreatment_Button = new System.Windows.Forms.Button();
			this.NotSpecifiedSimptom_Button = new System.Windows.Forms.Button();
			this.Diagnosis_TextBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// FirstName_Label
			// 
			this.FirstName_Label.AutoSize = true;
			this.FirstName_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.FirstName_Label.Location = new System.Drawing.Point(12, 10);
			this.FirstName_Label.Name = "FirstName_Label";
			this.FirstName_Label.Size = new System.Drawing.Size(106, 23);
			this.FirstName_Label.TabIndex = 0;
			this.FirstName_Label.Text = "First Name:";
			// 
			// LastName_Label
			// 
			this.LastName_Label.AutoSize = true;
			this.LastName_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.LastName_Label.Location = new System.Drawing.Point(12, 50);
			this.LastName_Label.Name = "LastName_Label";
			this.LastName_Label.Size = new System.Drawing.Size(105, 23);
			this.LastName_Label.TabIndex = 1;
			this.LastName_Label.Text = "Last Name:";
			// 
			// Age_Label
			// 
			this.Age_Label.AutoSize = true;
			this.Age_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.Age_Label.Location = new System.Drawing.Point(12, 90);
			this.Age_Label.Name = "Age_Label";
			this.Age_Label.Size = new System.Drawing.Size(53, 23);
			this.Age_Label.TabIndex = 3;
			this.Age_Label.Text = "Age: ";
			// 
			// Diagnosis_Label
			// 
			this.Diagnosis_Label.AutoSize = true;
			this.Diagnosis_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.Diagnosis_Label.Location = new System.Drawing.Point(12, 216);
			this.Diagnosis_Label.Name = "Diagnosis_Label";
			this.Diagnosis_Label.Size = new System.Drawing.Size(95, 23);
			this.Diagnosis_Label.TabIndex = 4;
			this.Diagnosis_Label.Text = "Diagnosis: ";
			// 
			// FirstName_TextBox
			// 
			this.FirstName_TextBox.Location = new System.Drawing.Point(140, 15);
			this.FirstName_TextBox.Multiline = true;
			this.FirstName_TextBox.Name = "FirstName_TextBox";
			this.FirstName_TextBox.Size = new System.Drawing.Size(123, 20);
			this.FirstName_TextBox.TabIndex = 1;
			// 
			// LastName_TextBox
			// 
			this.LastName_TextBox.Location = new System.Drawing.Point(140, 55);
			this.LastName_TextBox.Multiline = true;
			this.LastName_TextBox.Name = "LastName_TextBox";
			this.LastName_TextBox.Size = new System.Drawing.Size(123, 20);
			this.LastName_TextBox.TabIndex = 2;
			// 
			// Age_TextBox
			// 
			this.Age_TextBox.Location = new System.Drawing.Point(140, 95);
			this.Age_TextBox.Multiline = true;
			this.Age_TextBox.Name = "Age_TextBox";
			this.Age_TextBox.Size = new System.Drawing.Size(123, 20);
			this.Age_TextBox.TabIndex = 3;
			// 
			// OK_Button
			// 
			this.OK_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.OK_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.OK_Button.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
			this.OK_Button.Location = new System.Drawing.Point(180, 437);
			this.OK_Button.Name = "OK_Button";
			this.OK_Button.Size = new System.Drawing.Size(66, 34);
			this.OK_Button.TabIndex = 10;
			this.OK_Button.Text = "OK";
			this.OK_Button.UseVisualStyleBackColor = true;
			this.OK_Button.Click += new System.EventHandler(this.OK_Button_Click);
			// 
			// Simptoms_Label
			// 
			this.Simptoms_Label.AutoSize = true;
			this.Simptoms_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.Simptoms_Label.Location = new System.Drawing.Point(12, 258);
			this.Simptoms_Label.Name = "Simptoms_Label";
			this.Simptoms_Label.Size = new System.Drawing.Size(94, 23);
			this.Simptoms_Label.TabIndex = 12;
			this.Simptoms_Label.Text = "Simptoms:";
			// 
			// Simptoms_TextBox
			// 
			this.Simptoms_TextBox.Location = new System.Drawing.Point(140, 258);
			this.Simptoms_TextBox.Multiline = true;
			this.Simptoms_TextBox.Name = "Simptoms_TextBox";
			this.Simptoms_TextBox.Size = new System.Drawing.Size(123, 20);
			this.Simptoms_TextBox.TabIndex = 7;
			// 
			// Gender_Label
			// 
			this.Gender_Label.AutoSize = true;
			this.Gender_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.Gender_Label.Location = new System.Drawing.Point(12, 133);
			this.Gender_Label.Name = "Gender_Label";
			this.Gender_Label.Size = new System.Drawing.Size(82, 23);
			this.Gender_Label.TabIndex = 14;
			this.Gender_Label.Text = "Gender: ";
			// 
			// Gender_ComboBox
			// 
			this.Gender_ComboBox.FormattingEnabled = true;
			this.Gender_ComboBox.Items.AddRange(new object[] {
            "Male",
            "Female"});
			this.Gender_ComboBox.Location = new System.Drawing.Point(140, 135);
			this.Gender_ComboBox.Name = "Gender_ComboBox";
			this.Gender_ComboBox.Size = new System.Drawing.Size(121, 21);
			this.Gender_ComboBox.TabIndex = 4;
			// 
			// Residence_Label
			// 
			this.Residence_Label.AutoSize = true;
			this.Residence_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.Residence_Label.Location = new System.Drawing.Point(12, 172);
			this.Residence_Label.Name = "Residence_Label";
			this.Residence_Label.Size = new System.Drawing.Size(101, 23);
			this.Residence_Label.TabIndex = 16;
			this.Residence_Label.Text = "Residence: ";
			// 
			// Residence_TextBox
			// 
			this.Residence_TextBox.Location = new System.Drawing.Point(140, 177);
			this.Residence_TextBox.Multiline = true;
			this.Residence_TextBox.Name = "Residence_TextBox";
			this.Residence_TextBox.Size = new System.Drawing.Size(123, 20);
			this.Residence_TextBox.TabIndex = 5;
			// 
			// DateOfRegistration_Label
			// 
			this.DateOfRegistration_Label.AutoSize = true;
			this.DateOfRegistration_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.DateOfRegistration_Label.Location = new System.Drawing.Point(12, 301);
			this.DateOfRegistration_Label.Name = "DateOfRegistration_Label";
			this.DateOfRegistration_Label.Size = new System.Drawing.Size(181, 23);
			this.DateOfRegistration_Label.TabIndex = 18;
			this.DateOfRegistration_Label.Text = "Date of Registration:";
			// 
			// CurrentDate_Button
			// 
			this.CurrentDate_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.CurrentDate_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.CurrentDate_Button.Font = new System.Drawing.Font("Berlin Sans FB", 10F);
			this.CurrentDate_Button.Location = new System.Drawing.Point(337, 335);
			this.CurrentDate_Button.Name = "CurrentDate_Button";
			this.CurrentDate_Button.Size = new System.Drawing.Size(82, 23);
			this.CurrentDate_Button.TabIndex = 19;
			this.CurrentDate_Button.Text = "Data curenta";
			this.CurrentDate_Button.UseVisualStyleBackColor = true;
			this.CurrentDate_Button.Click += new System.EventHandler(this.CurrentDate_Button_Click);
			// 
			// Day_ComboBox
			// 
			this.Day_ComboBox.FormattingEnabled = true;
			this.Day_ComboBox.Location = new System.Drawing.Point(89, 337);
			this.Day_ComboBox.Name = "Day_ComboBox";
			this.Day_ComboBox.Size = new System.Drawing.Size(39, 21);
			this.Day_ComboBox.TabIndex = 20;
			// 
			// Month_ComboBox
			// 
			this.Month_ComboBox.FormattingEnabled = true;
			this.Month_ComboBox.Location = new System.Drawing.Point(140, 337);
			this.Month_ComboBox.Name = "Month_ComboBox";
			this.Month_ComboBox.Size = new System.Drawing.Size(87, 21);
			this.Month_ComboBox.TabIndex = 21;
			// 
			// Year_ComboBox
			// 
			this.Year_ComboBox.FormattingEnabled = true;
			this.Year_ComboBox.Location = new System.Drawing.Point(233, 337);
			this.Year_ComboBox.Name = "Year_ComboBox";
			this.Year_ComboBox.Size = new System.Drawing.Size(87, 21);
			this.Year_ComboBox.TabIndex = 22;
			// 
			// DD_Label
			// 
			this.DD_Label.AutoSize = true;
			this.DD_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.DD_Label.Location = new System.Drawing.Point(90, 361);
			this.DD_Label.Name = "DD_Label";
			this.DD_Label.Size = new System.Drawing.Size(38, 23);
			this.DD_Label.TabIndex = 23;
			this.DD_Label.Text = "DD";
			// 
			// MM_Label
			// 
			this.MM_Label.AutoSize = true;
			this.MM_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.MM_Label.Location = new System.Drawing.Point(166, 361);
			this.MM_Label.Name = "MM_Label";
			this.MM_Label.Size = new System.Drawing.Size(40, 23);
			this.MM_Label.TabIndex = 24;
			this.MM_Label.Text = "MM";
			// 
			// YY_Label
			// 
			this.YY_Label.AutoSize = true;
			this.YY_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.YY_Label.Location = new System.Drawing.Point(254, 361);
			this.YY_Label.Name = "YY_Label";
			this.YY_Label.Size = new System.Drawing.Size(34, 23);
			this.YY_Label.TabIndex = 25;
			this.YY_Label.Text = "YY";
			// 
			// Treatment_Label
			// 
			this.Treatment_Label.AutoSize = true;
			this.Treatment_Label.Font = new System.Drawing.Font("Berlin Sans FB", 15F);
			this.Treatment_Label.Location = new System.Drawing.Point(12, 399);
			this.Treatment_Label.Name = "Treatment_Label";
			this.Treatment_Label.Size = new System.Drawing.Size(103, 23);
			this.Treatment_Label.TabIndex = 26;
			this.Treatment_Label.Text = "Treatment:";
			// 
			// Treatment_TextBox
			// 
			this.Treatment_TextBox.Location = new System.Drawing.Point(140, 402);
			this.Treatment_TextBox.Multiline = true;
			this.Treatment_TextBox.Name = "Treatment_TextBox";
			this.Treatment_TextBox.Size = new System.Drawing.Size(123, 20);
			this.Treatment_TextBox.TabIndex = 27;
			// 
			// NotSpecifiedTreatment_Button
			// 
			this.NotSpecifiedTreatment_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.NotSpecifiedTreatment_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.NotSpecifiedTreatment_Button.Font = new System.Drawing.Font("Berlin Sans FB", 10F);
			this.NotSpecifiedTreatment_Button.Location = new System.Drawing.Point(279, 400);
			this.NotSpecifiedTreatment_Button.Name = "NotSpecifiedTreatment_Button";
			this.NotSpecifiedTreatment_Button.Size = new System.Drawing.Size(104, 23);
			this.NotSpecifiedTreatment_Button.TabIndex = 28;
			this.NotSpecifiedTreatment_Button.Text = "Nu este cazul.";
			this.NotSpecifiedTreatment_Button.UseVisualStyleBackColor = true;
			this.NotSpecifiedTreatment_Button.Click += new System.EventHandler(this.NotSpecified_Button_Click);
			// 
			// NotSpecifiedSimptom_Button
			// 
			this.NotSpecifiedSimptom_Button.Cursor = System.Windows.Forms.Cursors.Hand;
			this.NotSpecifiedSimptom_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.NotSpecifiedSimptom_Button.Font = new System.Drawing.Font("Berlin Sans FB", 10F);
			this.NotSpecifiedSimptom_Button.Location = new System.Drawing.Point(301, 258);
			this.NotSpecifiedSimptom_Button.Name = "NotSpecifiedSimptom_Button";
			this.NotSpecifiedSimptom_Button.Size = new System.Drawing.Size(104, 23);
			this.NotSpecifiedSimptom_Button.TabIndex = 8;
			this.NotSpecifiedSimptom_Button.Text = "Nu este cazul.";
			this.NotSpecifiedSimptom_Button.UseVisualStyleBackColor = true;
			this.NotSpecifiedSimptom_Button.Click += new System.EventHandler(this.NotSpecifiedSimptom_Button_Click);
			// 
			// Diagnosis_TextBox
			// 
			this.Diagnosis_TextBox.Location = new System.Drawing.Point(140, 221);
			this.Diagnosis_TextBox.Multiline = true;
			this.Diagnosis_TextBox.Name = "Diagnosis_TextBox";
			this.Diagnosis_TextBox.Size = new System.Drawing.Size(123, 20);
			this.Diagnosis_TextBox.TabIndex = 29;
			// 
			// NewPatient
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.ClientSize = new System.Drawing.Size(431, 483);
			this.Controls.Add(this.Diagnosis_TextBox);
			this.Controls.Add(this.NotSpecifiedSimptom_Button);
			this.Controls.Add(this.NotSpecifiedTreatment_Button);
			this.Controls.Add(this.Treatment_TextBox);
			this.Controls.Add(this.Treatment_Label);
			this.Controls.Add(this.YY_Label);
			this.Controls.Add(this.MM_Label);
			this.Controls.Add(this.DD_Label);
			this.Controls.Add(this.Year_ComboBox);
			this.Controls.Add(this.Month_ComboBox);
			this.Controls.Add(this.Day_ComboBox);
			this.Controls.Add(this.CurrentDate_Button);
			this.Controls.Add(this.DateOfRegistration_Label);
			this.Controls.Add(this.Residence_TextBox);
			this.Controls.Add(this.Residence_Label);
			this.Controls.Add(this.Gender_ComboBox);
			this.Controls.Add(this.Gender_Label);
			this.Controls.Add(this.Simptoms_TextBox);
			this.Controls.Add(this.Simptoms_Label);
			this.Controls.Add(this.OK_Button);
			this.Controls.Add(this.Age_TextBox);
			this.Controls.Add(this.LastName_TextBox);
			this.Controls.Add(this.FirstName_TextBox);
			this.Controls.Add(this.Diagnosis_Label);
			this.Controls.Add(this.Age_Label);
			this.Controls.Add(this.LastName_Label);
			this.Controls.Add(this.FirstName_Label);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "NewPatient";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "New Patient";
			this.Load += new System.EventHandler(this.NewPatient_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label FirstName_Label;
        private System.Windows.Forms.Label LastName_Label;
        private System.Windows.Forms.Label Age_Label;
        private System.Windows.Forms.Label Diagnosis_Label;
        private System.Windows.Forms.TextBox FirstName_TextBox;
        private System.Windows.Forms.TextBox LastName_TextBox;
        private System.Windows.Forms.TextBox Age_TextBox;
        private System.Windows.Forms.Button OK_Button;
        private System.Windows.Forms.Label Simptoms_Label;
        private System.Windows.Forms.TextBox Simptoms_TextBox;
        private System.Windows.Forms.Label Gender_Label;
        private System.Windows.Forms.ComboBox Gender_ComboBox;
        private System.Windows.Forms.Label Residence_Label;
        private System.Windows.Forms.TextBox Residence_TextBox;
        private System.Windows.Forms.Label DateOfRegistration_Label;
        private System.Windows.Forms.Button CurrentDate_Button;
        private System.Windows.Forms.ComboBox Day_ComboBox;
        private System.Windows.Forms.ComboBox Month_ComboBox;
        private System.Windows.Forms.ComboBox Year_ComboBox;
        private System.Windows.Forms.Label DD_Label;
        private System.Windows.Forms.Label MM_Label;
        private System.Windows.Forms.Label YY_Label;
        private System.Windows.Forms.Label Treatment_Label;
        private System.Windows.Forms.TextBox Treatment_TextBox;
        private System.Windows.Forms.Button NotSpecifiedTreatment_Button;
        private System.Windows.Forms.Button NotSpecifiedSimptom_Button;
        private System.Windows.Forms.TextBox Diagnosis_TextBox;
    }
}