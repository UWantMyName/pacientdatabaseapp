﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacientDatabaseApp
{
    public partial class MainMenu : Form
    {
        public bool English = false;
        public string ConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='E:\STEVEN STUFF\PROGRAMARE\C#\PACIENTDATABASEAPP\PacientDatabaseApp\PacientDatabaseApp\Patients.mdf';Integrated Security = True";
        public SqlConnection Connection;

        public MainMenu()
        {
            InitializeComponent();
            
        }

        private void Quit_Button_Click(object sender, EventArgs e)
        {
            //Connection.Close();
            Application.Exit();
        }

        private void NewPatient_Button_Click(object sender, EventArgs e)
        {
            NewPatient f = new NewPatient();
            f.Show(this);
        }

        private void PatientsList_Button_Click(object sender, EventArgs e)
        {
            PatientList f = new PatientList();

            f.Show();
        }

        private void Credits_Label_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Programmer: Stefan Tanasa.\nConcept: Stefan Tanasa.");
        }
    }
}
