﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using PacientDatabaseApp.Properties;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacientDatabaseApp
{
    public partial class PatientList : Form
    {
		string ConnectionString;// = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='D:\STEVEN STUFF\PROGRAMARE\C#\PACIENTDATABASEAPP\PacientDatabaseApp\PacientDatabaseApp\Patients.mdf';Integrated Security=True";

        public PatientList()
        {
            InitializeComponent();

			string path = Path.GetDirectoryName(Path.GetDirectoryName(Application.StartupPath));
			ConnectionString = "Data Source=(LocalDB)" + "\\" + "MSSQLLocalDB;AttachDbFilename=" + "\"" + path + "\\Patients.mdf" + "\"" + ";Integrated Security=True";

			LoadForm();
        }

        private bool FiltersEmpty()
        {
            if (FirstName_TextBox.Text.Length > 0) return false;
            if (LastName_TextBox.Text.Length > 0) return false;
            if (Age_TextBox.Text.Length > 0) return false;
            if (Gender_ComboBox.Text.Length > 0) return false;
            if (Residence_TextBox.Text.Length > 0) return false;
            if (Simptoms_TextBox.Text.Length > 0) return false;
            if (Diagnosis_TextBox.Text.Length > 0) return false;
            if (Day_ComboBox.Text.Length > 0) return false;
            if (Month_ComboBox.Text.Length > 0) return false;
            if (Year_ComboBox.Text.Length > 0) return false;
            if (Treatment_TextBox.Text.Length > 0) return false;
            return true;
        }

        private void LoadForm()
        {
            int i;

            for (i = 1; i <= 31; i++)
                Day_ComboBox.Items.Add(i.ToString());

            Month_ComboBox.Items.Add("Ianuarie");
            Month_ComboBox.Items.Add("Februarie");
            Month_ComboBox.Items.Add("Martie");
            Month_ComboBox.Items.Add("Aprilie");
            Month_ComboBox.Items.Add("Mai");
            Month_ComboBox.Items.Add("Iunie");
            Month_ComboBox.Items.Add("Iulie");
            Month_ComboBox.Items.Add("August");
            Month_ComboBox.Items.Add("Septembrie");
            Month_ComboBox.Items.Add("Octombrie");
            Month_ComboBox.Items.Add("Noiembrie");
            Month_ComboBox.Items.Add("Decembrie");

            for (i = 2000; i <= DateTime.Today.Year; i++)
                Year_ComboBox.Items.Add(i.ToString());
        }

        private void ShowAll_Button_Click(object sender, EventArgs e)
        {
            SqlCommand command = new SqlCommand();
            bool added = false;

            if (FiltersEmpty()) command.CommandText = "select * from Patients;";
            else
            {
                command.CommandText = "select * from Patients where "; ;
                
                if (FirstName_TextBox.Text.Length > 0)
                {
                    command.CommandText += ("FirstName=\'" + FirstName_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (LastName_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("LastName=\'" + LastName_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Age_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Age=\'" + int.Parse(Age_TextBox.Text) + "\'");
                    if (!added) added = true;
                }
                if (Gender_ComboBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    if (Gender_ComboBox.Text == "Male") command.CommandText += ("Gender=1");
                    else command.CommandText += ("Gender=0");
                    if (!added) added = true;
                }
                if (Residence_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Residence=\'" + Residence_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Diagnosis_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Diagnosis=\'" + Diagnosis_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Simptoms_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Simptoms=\'" + Simptoms_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Day_ComboBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("DayOR=\'" + int.Parse(Day_ComboBox.Text) + "\'");
                    if (!added) added = true;
                }
                if (Month_ComboBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("MonthOR=\'" + Month_ComboBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Year_ComboBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("YearOR=\'" + int.Parse(Year_ComboBox.Text) + "\'");
                    if (!added) added = true;
                }
                if (Treatment_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Treatment=\'" + Treatment_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                command.CommandText += ";";
            }

            using (var Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                using (SqlCommand com = new SqlCommand(command.CommandText,Connection))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter(com))
                    {
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            patientsDataGridView.DataSource = dt;
                        }
                    }
                }
                Connection.Close();
            }
        }

        private void Update_Button_Click(object sender, EventArgs e)
        {
            bool added = false;
            SqlCommand command = new SqlCommand();

            if (FiltersEmpty()) MessageBox.Show("Complete at least one filter to update a patient.");
            else
            {
                command.CommandText = "update Patients set ";

                if (FirstName_TextBox.Text.Length > 0)
                {
                    command.CommandText += ("FirstName=\'" + FirstName_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (LastName_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("LastName=\'" + LastName_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Age_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Age=\'" + int.Parse(Age_TextBox.Text) + "\'");
                    if (!added) added = true;
                }
                if (Gender_ComboBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    if (Gender_ComboBox.Text == "Male") command.CommandText += ("Gender=1");
                    else command.CommandText += ("Gender=0");
                    if (!added) added = true;
                }
                if (Residence_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Residence=\'" + Residence_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Diagnosis_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Diagnosis=\'" + Diagnosis_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Simptoms_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Simptoms=\'" + Simptoms_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Day_ComboBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("DayOR=\'" + int.Parse(Day_ComboBox.Text) + "\'");
                    if (!added) added = true;
                }
                if (Month_ComboBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("MonthOR=\'" + Month_ComboBox.Text + "\'");
                    if (!added) added = true;
                }
                if (Year_ComboBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("YearOR=\'" + int.Parse(Year_ComboBox.Text) + "\'");
                    if (!added) added = true;
                }
                if (Treatment_TextBox.Text.Length > 0)
                {
                    if (added) command.CommandText += " and ";
                    command.CommandText += ("Treatment=\'" + Treatment_TextBox.Text + "\'");
                    if (!added) added = true;
                }
                command.CommandText += ("where Id = " + int.Parse(IdPatient_TextBox.Text) + ";");
            }

            using (var Connection = new SqlConnection(ConnectionString))
            {
                Connection.Open();
                using (SqlCommand com = new SqlCommand(command.CommandText, Connection))
                {
                    com.ExecuteNonQuery();
                }
                Connection.Close();
            }
        }
    }
}
