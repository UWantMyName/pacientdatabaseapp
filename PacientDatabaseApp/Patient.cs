﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PacientDatabaseApp
{
    public class Patient
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public int Gender { get; set; }
        public string Residence { get; set; }
        public string Simptoms { get; set; }
        public string Diagnosis { get; set; }
        public int DayOR { get; set; }
        public string MonthOR { get; set; }
        public int YearOR { get; set; }
        public string Treatment { get; set; }
    }
}
