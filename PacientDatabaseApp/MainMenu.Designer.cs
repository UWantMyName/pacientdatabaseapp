﻿namespace PacientDatabaseApp
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.Quit_Button = new System.Windows.Forms.Button();
            this.NewPatient_Button = new System.Windows.Forms.Button();
            this.PatientsList_Button = new System.Windows.Forms.Button();
            this.PatientManager_Label = new System.Windows.Forms.Label();
            this.Credits_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Quit_Button
            // 
            this.Quit_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Quit_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Quit_Button.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.Quit_Button.Location = new System.Drawing.Point(192, 167);
            this.Quit_Button.Name = "Quit_Button";
            this.Quit_Button.Size = new System.Drawing.Size(66, 34);
            this.Quit_Button.TabIndex = 1;
            this.Quit_Button.Text = "Quit";
            this.Quit_Button.UseVisualStyleBackColor = true;
            this.Quit_Button.Click += new System.EventHandler(this.Quit_Button_Click);
            // 
            // NewPatient_Button
            // 
            this.NewPatient_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.NewPatient_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.NewPatient_Button.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.NewPatient_Button.Location = new System.Drawing.Point(12, 50);
            this.NewPatient_Button.Name = "NewPatient_Button";
            this.NewPatient_Button.Size = new System.Drawing.Size(168, 38);
            this.NewPatient_Button.TabIndex = 2;
            this.NewPatient_Button.Text = "New Patient";
            this.NewPatient_Button.UseVisualStyleBackColor = true;
            this.NewPatient_Button.Click += new System.EventHandler(this.NewPatient_Button_Click);
            // 
            // PatientsList_Button
            // 
            this.PatientsList_Button.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PatientsList_Button.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.PatientsList_Button.Font = new System.Drawing.Font("Berlin Sans FB", 12F);
            this.PatientsList_Button.Location = new System.Drawing.Point(12, 110);
            this.PatientsList_Button.Name = "PatientsList_Button";
            this.PatientsList_Button.Size = new System.Drawing.Size(168, 38);
            this.PatientsList_Button.TabIndex = 3;
            this.PatientsList_Button.Text = "Patient List";
            this.PatientsList_Button.UseVisualStyleBackColor = true;
            this.PatientsList_Button.Click += new System.EventHandler(this.PatientsList_Button_Click);
            // 
            // PatientManager_Label
            // 
            this.PatientManager_Label.AutoSize = true;
            this.PatientManager_Label.Font = new System.Drawing.Font("Berlin Sans FB", 18F);
            this.PatientManager_Label.Location = new System.Drawing.Point(45, 9);
            this.PatientManager_Label.Name = "PatientManager_Label";
            this.PatientManager_Label.Size = new System.Drawing.Size(182, 26);
            this.PatientManager_Label.TabIndex = 4;
            this.PatientManager_Label.Text = "Patient Manager";
            // 
            // Credits_Label
            // 
            this.Credits_Label.AutoSize = true;
            this.Credits_Label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Credits_Label.Font = new System.Drawing.Font("Berlin Sans FB", 18F);
            this.Credits_Label.Location = new System.Drawing.Point(12, 175);
            this.Credits_Label.Name = "Credits_Label";
            this.Credits_Label.Size = new System.Drawing.Size(80, 26);
            this.Credits_Label.TabIndex = 5;
            this.Credits_Label.Text = "Credits";
            this.Credits_Label.Click += new System.EventHandler(this.Credits_Label_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(270, 212);
            this.ControlBox = false;
            this.Controls.Add(this.Credits_Label);
            this.Controls.Add(this.PatientManager_Label);
            this.Controls.Add(this.PatientsList_Button);
            this.Controls.Add(this.NewPatient_Button);
            this.Controls.Add(this.Quit_Button);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Patient Manager";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Quit_Button;
        private System.Windows.Forms.Button NewPatient_Button;
        private System.Windows.Forms.Button PatientsList_Button;
        private System.Windows.Forms.Label PatientManager_Label;
        private System.Windows.Forms.Label Credits_Label;
    }
}

